package nebulaisAFunnyName.ScheduleDaytime.controller;

import java.time.LocalDate;
import java.util.ArrayList;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import nebulaisAFunnyName.ScheduleDaytime.dal.PersonDal;
import nebulaisAFunnyName.ScheduleDaytime.entity.Person;

@Controller
@RequestMapping(value="/admin/person")
public class HomeController {
	PersonDal persondal = new PersonDal();
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home()
	{
		
		
		return "home";
	}
	
	
	////Ki�i ekleme
	@RequestMapping(value = "/person", method = RequestMethod.GET) 
							//Giri� ekran� yap�l�p Token verildikten sonra metod post'a �ekilecek
	public String addUser(ModelMap modelMap)
	{
		modelMap.addAttribute("person", new Person()); 
		//Buradaki person nesnesini i�aret eden "person" string'i ile person.jsp sayfas�ndaki form'un 
		//modelAttribute name'i ayn� olmal�
		
		return "person";
		
	}
	
	/*Yukarki metodun view'dan username'i alabilmesi i�in yapt�m, tabi eksik
	 *  https://www.youtube.com/watch?v=zyNcRRBpBg8
	@RequestMapping(value = "/person", method = RequestMethod.GET) 
							//Giri� ekran� yap�l�p Token verildikten sonra metod post'a �ekilecek 
	public ModelAndView addUser(ModelMap modelMap)
	{
		modelMap.addAttribute("command", new Person());
		//Sonradan eklenen
		Person person = new Person();
		ModelAndView mav = new ModelAndView("person", "command", person);
		mav.addObject("PersonForm");
		//--
		
		return mav;
	}
	 */
	
	@RequestMapping(value="addPerson", method = RequestMethod.POST)
	public String addPerson(@ModelAttribute("newPerson") Person person , ModelMap modelMap)
	{
		person.setCreationTime(LocalDate.now());
		person.setUpdatingTime(LocalDate.now());
		long id = persondal.insertPerson(person); //sayfaya gelen model al�nd� ve vt'ye g�nderildi
		
		modelMap.addAttribute("id", id);
		modelMap.addAttribute("userName", person.getUserName());
		modelMap.addAttribute("password", person.getPassword());
		modelMap.addAttribute("email", person.getEmail());
		modelMap.addAttribute("creationTime", person.getCreationTime());
		modelMap.addAttribute("updatingTime", person.getUpdatingTime());
		
		if(person.getId() == 0L)
		{
			return "error";
		}
		
		return "addedPerson";
	}
	
	//Liste alma
	@RequestMapping(value="/listPersons", method = RequestMethod.GET)
	public String getAll(ModelMap modelMap)
	{
		ArrayList<Person> persons = new ArrayList<Person>();
		persons = persondal.getAllPerson();
		//Taglib'le jsp i�ersinde bo� mu kontrol� yapt�m ancak do�ru bir yakla��m olmayabilir. 
		//O y�zden burada da yap�yorum ve �imdilik bo�sa error sayfas�na y�nlediriyorum
		if(persons.size() <= 0 )
		{
			return "error";
		}
		
		
		modelMap.addAttribute("persons", persons);
		
		return "personList";
		
	}
	
	//Bulma
	@RequestMapping(value="/findPerson", method = RequestMethod.GET) //�lerde bu metod post olabilir
	public String findPersonById(ModelMap model)
	{
		//Person person = new Person();
		model.addAttribute("findPerson", new Person());
		
		return "findPerson";
	}
	
	@RequestMapping(value="/getPerson", method = RequestMethod.POST)
	public String getPerson(@ModelAttribute("getPerson") Person person, ModelMap modelMap)
	{
		Person pers;
		pers = persondal.getPersonByID(person.getId()); //�d bo� gelirse de�eri 0 olur, bu sefer string aramas� yapar
				
		if(person.getId() == 0L)//id kutusundan de�er gelmezse, string kutucu�una bak�l�r.
		{
			pers = persondal.getPersonByString(person.getUserName());
		}
		
		if(pers.getId() == 0L) //ne string ne de id ile de�er alamazsan error'a y�nlendirir.
		{
			return "error";
		}
		
		modelMap.addAttribute("userName", pers.getUserName());
		modelMap.addAttribute("id", pers.getId());
		modelMap.addAttribute("password", pers.getPassword());
		modelMap.addAttribute("email", pers.getEmail());
		modelMap.addAttribute("creationTime", pers.getCreationTime());
		modelMap.addAttribute("updatingTime", pers.getUpdatingTime());
		return "getPerson";
	}
	
	//Silme
	@RequestMapping(value="/deletePerson", method = RequestMethod.GET)
	public String deletePerson(ModelMap modelMap) 
	{
		modelMap.addAttribute("command", new Person());
		
		return "deletePerson";
	}
	
	@RequestMapping(value="/deletedPerson", method = RequestMethod.POST)
	public String deletePerson(@ModelAttribute("deletePerson") Person person, ModelMap modelMap) 
	{
		boolean status;
		status = persondal.deletePersonById(person.getId());
		if(person.getId() == 0L)
		{
			status = persondal.deletePersonByName(person.getUserName()); 
		}
		
		if(status)
		{
			return "successDeleting";
		}
		else
		{
			return "error";
		}
	}
	
	//G�ncelleme
	@RequestMapping(value = "/updatePerson", method = RequestMethod.GET)
	public String updatePerson(ModelMap modelMap)
	{
		modelMap.addAttribute("command", new Person());
		
		return "updatePerson";
	}
	
	@RequestMapping(value = "/updatingPerson", method = RequestMethod.POST)
	public String updatingPerson(@ModelAttribute("updatingPerson") Person person, ModelMap modelMap)
	{
		Person newPerson;
		newPerson= persondal.getPersonByID(person.getId());
		if(newPerson.getId() == 0L)
		{
			newPerson = persondal.getPersonByString(person.getUserName());
		}

		if(newPerson == null)
		{
			return "error";
		}
		//CreationTime, updatingPerson sayfas�na hidden olarak ge�ti. Bunun kontrollerinin yaz�lmas� gerekiyor.
		modelMap.addAttribute("id", newPerson.getId());
		modelMap.addAttribute("userName", newPerson.getUserName());
		modelMap.addAttribute("password", newPerson.getPassword());
		modelMap.addAttribute("email", newPerson.getEmail());
		modelMap.addAttribute("creationTime", newPerson.getCreationTime());
		modelMap.addAttribute("updatingTime", newPerson.getUpdatingTime());	
		modelMap.addAttribute("command", newPerson); //kutucuktan girilecek Person de�erini alacak, di�er sayfaya g�nderecek.
		
		return "updatingPerson";
	}
	
	@RequestMapping(value = "/updatedPerson", method = RequestMethod.POST)
	public String updatedPerson(@ModelAttribute("updatedPerson") Person person, ModelMap modelMap)
	{
		if(person == null)
		{
			return "error";
		}
		
		person.setUpdatingTime(LocalDate.now());
		persondal.updatePerson(person.getId(), person);
		modelMap.addAttribute("id", person.getId());
		modelMap.addAttribute("userName", person.getUserName());
		modelMap.addAttribute("password", person.getPassword());
		modelMap.addAttribute("email", person.getEmail());
		modelMap.addAttribute("creationTime", person.getCreationTime());
		modelMap.addAttribute("updatingTime", person.getUpdatingTime());
		modelMap.addAttribute("command", person);
		
		return "updatedPerson";
	}
	
	//Update i�lemi, id uri uzantl� Get metodu ile
	//Hat�rlatma: Hata al�rsan, value'yi de�i�tirmeyi dene
	@RequestMapping(value="/updatePersons/{id}", method = RequestMethod.GET)
	public ModelAndView updatePersonById(@PathVariable long id, ModelMap modelMap)
	{
		Person person = persondal.getPersonByID(id);
		ModelAndView mv = new ModelAndView("updatingPersonForId", "command", person);
		
		return mv;
	}
}
