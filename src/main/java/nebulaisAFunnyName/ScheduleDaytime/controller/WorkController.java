package nebulaisAFunnyName.ScheduleDaytime.controller;

import java.util.ArrayList;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import nebulaisAFunnyName.ScheduleDaytime.dal.WorkDal;
import nebulaisAFunnyName.ScheduleDaytime.entity.Work;

@Controller
@RequestMapping(value = "/admin/work")
public class WorkController {
	WorkDal workdal = new WorkDal();
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home()
	{
		
		
		return "homeWork";
	}
	
	//Ekleme
	@RequestMapping(value= "/addWork", method = RequestMethod.GET)
	public ModelAndView getWork()
	{
		Work work = new Work();
		
		return new ModelAndView("addWork", "command", work);
	}
	
	@RequestMapping(value = "/addedWork", method = RequestMethod.POST)
	public String addWork(@ModelAttribute("newWork") Work work, ModelMap modelMap)
	{
		workdal.insertWork(work);
		
		modelMap.addAttribute("id", work.getId()); //�lerde kald�r�lacak
		modelMap.addAttribute("name", work.getName());
		modelMap.addAttribute("date", work.getDate());
		
		return "addedWork";
	}
	
	//Listeme
	@RequestMapping(value = "/listWork", method = RequestMethod.GET)
	public String getWorks(ModelMap modelMap)
	{
		ArrayList<Work> works = new ArrayList<Work>();
		works = workdal.getAllWorks();
		modelMap.addAttribute("works", works);
		
		return "listWork";
	}
	
	//Silme
	@RequestMapping(value= "/deleteWork", method = RequestMethod.GET)
	public ModelAndView deleteWork()
	{
		Work work = new Work();
		return new ModelAndView("deleteWork", "command", work);
	}
	
	@RequestMapping(value = "/deletedWork", method = RequestMethod.POST)
	public String deletedWork(@ModelAttribute("removeWork") Work work, ModelMap modelMap)
	{
		boolean status = workdal.deleteWorkById(work.getId());
		//Silinen kullan�c� bilgilerini g�stermek i�in:
		modelMap.addAttribute("id", work.getId());
		modelMap.addAttribute("name", work.getName());
		modelMap.addAttribute("date", work.getDate());
		
		if(status)
		{
			return "deletedWork";
		}
		return "error";
	}
	
	//G�ncelleme
	@RequestMapping(value= "/updateWork", method = RequestMethod.GET)
	public ModelAndView updateWork() 
	//modelView �imdilik kullan�yorum �zerinde �al��t���m i�in.Ama gereksiz
	//��nk� b�t�n bir modeli ge�mek yerine bir id bilgisi ge�erek bunu yapabilirim
	{
		Work work = new Work();
		return new ModelAndView("updateWork", "command", work);
	}
	
	@RequestMapping(value = "/updatingWork", method = RequestMethod.POST)
	public ModelAndView updatingWork(@ModelAttribute("changeWork") Work work, ModelMap modelMap)
	{
		Work oldWork = workdal.getWorkById(work.getId());
		//modelMap.addAttribute("id", oldWork.getId()); //��nk� de�i�meyecek, id bilgisi
		modelMap.addAttribute("name", oldWork.getName());
		modelMap.addAttribute("date", oldWork.getDate());
		//Sonradan a�a��daki return'� kapat�p bunu da denemeyi unutma 
		//return new ModelAndView("updatingWork", "command", new newWork());
		return new ModelAndView("updatingWork", "command", oldWork);		
	}
	
	
	@RequestMapping(value = "/updatedWork", method = RequestMethod.POST)
	public String updatedWork(@ModelAttribute("changeWork") Work work, ModelMap modelMap)
	{
		boolean status = workdal.updateWork(work.getId(), work);
		//Silinen kullan�c� bilgilerini g�stermek i�in:
		modelMap.addAttribute("id", work.getId());
		modelMap.addAttribute("name", work.getName());
		modelMap.addAttribute("date", work.getDate());
		
		if(status)
		{
			return "udatedWork";
		}
		return "error";
	}
	
	//Bulma
	@RequestMapping(value= "/searchWork", method = RequestMethod.GET)
	public ModelAndView sarchWork()
	{
		Work work = new Work();
		return new ModelAndView("searchWork", "command", work);
	}
	
	@RequestMapping(value = "/findWork", method = RequestMethod.POST)
	public String findWork(@ModelAttribute("findWork") Work work, ModelMap modelMap)
	{
		Work findedWork = new Work();
		findedWork = workdal.getWorkById(work.getId());
		//Silinen kullan�c� bilgilerini g�stermek i�in:
		modelMap.addAttribute("id", findedWork.getId());
		modelMap.addAttribute("name", findedWork.getName());
		modelMap.addAttribute("date", findedWork.getDate());
		
		if(findedWork.getId() == 0L)
		{
			return "error";
		}
		return "findWork";
	}
}
