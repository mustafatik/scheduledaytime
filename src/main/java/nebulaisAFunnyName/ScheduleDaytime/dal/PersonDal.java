package nebulaisAFunnyName.ScheduleDaytime.dal;

import java.util.ArrayList;

import nebulaisAFunnyName.ScheduleDaytime.dao.PersonDao;
import nebulaisAFunnyName.ScheduleDaytime.entity.Person;

public class PersonDal {
	private final static String URL = "jdbc:postgresql://localhost:5432/postgres";
	private final static String USERNAME = "postgres";
	private final static String PASSWORD = "asdf";
	
	private PersonDao m_personDao = PersonDao.instance(URL, USERNAME, PASSWORD);
	
	
	public long insertPerson(Person person)
	{
		//Bu long t�r�n� her yerde boolean veya int'e �evirmeyi unutma
		long status = 0L;
		
		try {
			status = m_personDao.insert(person);	
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
		return status;
	}
	
	public ArrayList<Person> getAllPerson()
	{
		ArrayList<Person> persons = new ArrayList<Person>();
		
		try {
			persons = m_personDao.getAll();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
		return persons;
	}
	
	public Person getPersonByID(long id)
	{
		Person person = new Person();
		
		try {
			person = m_personDao.getPersonById(id);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
		return person;
	}
	
	public Person getPersonByString(String name)
	{
		Person person = new Person();
		
		try {
			person = m_personDao.getPersonByName(name);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
		return person;
	}
	
	public boolean deletePersonById(long id)
	{
		boolean status = false;
		
		try {
			status = m_personDao.deletePersonById(id);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
		return status;
	}
	
	public boolean deletePersonByName(String name)
	{
		boolean status = false;
		
		try {
			status = m_personDao.deletePersonByName(name);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
		return status;
	}
	
	public boolean updatePersonById(long id, String newUserName)
	{
		boolean status = false;
		
		try {
			status = m_personDao.updatePersonById(id, newUserName);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
		return status;
	}
	
	public boolean updatePersonByName(String oldUserName, String newUserName)
	{
		boolean status = false;
		
		try {
			status = m_personDao.updatePersonByName(oldUserName, newUserName);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
		return status;
	}
	
	public boolean updatePerson(long id , Person person)
	{
		boolean status = false;
		
		try {
			status = m_personDao.updatePerson(id, person);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
		return status;
	}
}
