package nebulaisAFunnyName.ScheduleDaytime.dal;

import nebulaisAFunnyName.ScheduleDaytime.dao.PersonWorkDao;
import nebulaisAFunnyName.ScheduleDaytime.entity.PersonWork;

public class PersonWorkDal {
	private final static String URL = "jdbc:postgresql://localhost:5432/postgres";
	private final static String USERNAME = "postgres";
	private final static String PASSWORD = "asdf";
	
	PersonWorkDao pw = PersonWorkDao.instance(URL, USERNAME, PASSWORD);
	
	public long insert(PersonWork personWork)
	{
		long status = 0L;
		try {
			status = pw.insert(personWork);
			
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		
		return status;
	}

}
