package nebulaisAFunnyName.ScheduleDaytime.dal;

import java.time.LocalDate;
import java.util.ArrayList;

import nebulaisAFunnyName.ScheduleDaytime.dao.WorkDao;
import nebulaisAFunnyName.ScheduleDaytime.entity.Work;

public class WorkDal {
	private final static String URL = "jdbc:postgresql://localhost:5432/postgres";
	private final static String USERNAME = "postgres";
	private final static String PASSWORD = "asdf";
	
	WorkDao m_workDao = WorkDao.getInstance(URL, USERNAME, PASSWORD);
	
	public long insertWork(Work work)
	{
		long status = 0L;
		
		try {
			status = m_workDao.insert(work);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
		return status;
	}
	
	public ArrayList<Work> getAllWorks()
	{
		ArrayList<Work> works = new ArrayList<Work>();
		try
		{			
			works = m_workDao.getAll();
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
		return works;
	}
	
	public Work getWorkById(long id)
	{
		Work work = new Work();
		
		try
		{
			work = m_workDao.getWorkById(id);
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
		return work;
	}
	
	public boolean deleteWorkById(long id)
	{
		boolean status = false;
		
		try
		{
			status = m_workDao.deleteWorkById(id);
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return status;
	}
	
	public boolean updateWorkNameById(long id, String newName)
	{
		boolean status = false;
		
		try
		{
			status = m_workDao.updateWorkNameById(id, newName);
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return status;
	}
	
	public boolean updateWorkDateById(long id, LocalDate newDate)
	{
		boolean status = false;
		
		try
		{
			status = m_workDao.updateWorkDateById(id, newDate);
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return status;
	}
	
	public boolean updateWork(long id, Work newWork)
	{
		boolean status = false;
		
		try
		{
			status = m_workDao.updateWork(id, newWork);
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return status;
	}

}
