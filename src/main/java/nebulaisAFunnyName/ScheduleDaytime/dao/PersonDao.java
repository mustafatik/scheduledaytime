package nebulaisAFunnyName.ScheduleDaytime.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import nebulaisAFunnyName.ScheduleDaytime.entity.Person;

public class PersonDao {
	//Singleton yap�l�yor
	private static final PersonDao ms_instance = new PersonDao();
	private String m_url, m_username, m_password;
	
	private static final String CMD_INSERT_PERSON = "insert into \"Persons\" ( \"UserName\", \"Password\", \"Email\", \"CreationTime\", \"UpdatingTime\" ) Values ( ?, ?, ?, ?, ?)";
	private static final String CMD_GETALL_PERSON = "select * from \"Persons\"";
	private static final String CMD_SEARCH_BY_ID = "select * from \"Persons\" where \"PersonId\" = ? ";
	private static final String CMD_SEARCH_BY_NAME = "select * from \"Persons\" where \"UserName\" like ? ";
	//String sorgusu yaparken, postgrede tek t�rnak ' ile stringi yazars�n. Burada like'dan sonra tek t�rkan koymazs�n.
	private static final String CMD_DELETE_BY_ID = "delete from \"Persons\" where \"PersonId\" = ?"; 
	private static final String CMD_DELETE_BY_NAME = "delete from \"Persons\" where \"UserName\" like ?"; 
	private static final String CMD_UPDATE_BY_ID = "update \"Persons\" set \"UserName\" = ?, \"Password\" = ?, \"Email\" = ?, " 
			+"\"CreationTime\" = ?, \"UpdatingTime\" = ? where \"PersonId\" = ?";
	private static final String CMD_UPDATE_BY_NAME = "update \"Persons\" set \"UserName\" = ?, \"Password\" = ?, \"Email\" = ?, " 
			+"\"CreationTime\" = ?, \"UpdatingTime\" = ? where \"UserName\"  like ?";
	private static final String CMD_PERSON_UPDATE = "update \"Persons\" set \"UserName\" = ?, \"Password\" = ?, \"Email\" = ?, "
			+ "\"CreationTime\" = ?, \"UpdatingTime\" = ? where \"PersonId\" = ?";
	
	private PersonDao()
	{
		
	}
	
	public static PersonDao instance(String url, String username, String password)
	{
		ms_instance.m_url = url;
		ms_instance.m_username = username;
		ms_instance.m_password = password;
		
		return ms_instance;
	}
	
	public Person getPerson(ResultSet rs) throws SQLException
	{
		Person p = new Person();
//		p.setId(rs.getLong(1));
//		p.setUserName(rs.getString(2));
		//Hata olu�ursa bunu kullan:
		p.setId(rs.getLong(2));
		p.setUserName(rs.getString(1));
		p.setPassword(rs.getString(3));
		p.setEmail(rs.getString(4));
		
		//�NEML�: 
		//BU if'i �u y�zden yazd�k
		//E�er tarih null ise(�nceki kay�tlardan dolay�) null de�eri toLocalDate'e �evirmeye �al��acak ve NullPointerException verecek
		//Biz bu time alanlar�n� not null yapt���m�zda a�a��daki ifadeyi if olmadan kullanaca��z
		//��nk� rs.getDate(5) ve rs.getDate(6) nulla gelmeyecek. olmayan de�erin de toLocalDate'ini almaya �al��mayaca��z.
		if(rs.getDate(5) != null)
		{
			p.setCreationTime(rs.getDate(5).toLocalDate());
			
		}
		
		if(rs.getDate(6) != null)
		{
			p.setUpdatingTime(rs.getDate(6).toLocalDate());
		}
		return p;
	}

	
	public Long insert(Person person)
	{
		String[] generatedKey = {"PersonId"};//"V.t'deki Id alan�n� de�i�tirdim 
											//o y�zden Otomatik �retilen column'un ad�na da PersonId verdim"
		long id = 0;
		
		try(Connection con = DriverManager.getConnection(m_url, m_username, m_password);
				PreparedStatement stmt = con.prepareStatement(CMD_INSERT_PERSON, generatedKey))
		{
			stmt.setString(1, person.getUserName());
			stmt.setString(2, person.getPassword());
			stmt.setString(3, person.getEmail());
			stmt.setDate(4, Date.valueOf(person.getCreationTime()));
			stmt.setDate(5, Date.valueOf(person.getUpdatingTime()));
			stmt.executeUpdate();
			
			ResultSet rs = stmt.getGeneratedKeys();
			if(rs.next())
			{
				id = rs.getLong(1);
			}
			
			person.setId(id);		
		}
		catch(SQLException sqle)
		{
			sqle.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
		return Long.valueOf(id);
	}
	
	public ArrayList<Person> getAll()
	{
		//List'le almaya ba�lad�m. Ama setle al�rs�n
		ArrayList<Person> persons = new ArrayList<Person>();
		
		try(Connection con = DriverManager.getConnection(m_url, m_username, m_password);
				PreparedStatement stmt = con.prepareStatement(CMD_GETALL_PERSON))
		{
			ResultSet rs = stmt.executeQuery();
			
			while(rs.next())
			{
				persons.add(getPerson(rs));				
			}
		}
		catch(SQLException sexc)
		{
			sexc.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return persons;
	}
	
	public Person getPersonById(long id)
	{
		Person person = new Person();
		
		try(Connection con = DriverManager.getConnection(m_url, m_username, m_password);
				PreparedStatement stmt = con.prepareStatement(CMD_SEARCH_BY_ID))
		{
			stmt.setLong(1, id);
			ResultSet rs = stmt.executeQuery();
			
			while(rs.next()) //Evet �unu ��rendik. ResultSet'i kullanacaksan bunu da kullanacaks�n.
			{
				person = getPerson(rs);
				//A�a��daki i�lemleri getPerson() metoduna yapt�rd�k.
//				person.setId(rs.getLong(1));
//				person.setUserName(rs.getString(2));
				
			}
			
		}
		catch(SQLException sexc)
		{
			sexc.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return person;
	}
	
	public Person getPersonByName(String name)
	{
		Person person = new Person();
		
		try(Connection con = DriverManager.getConnection(m_url, m_username, m_password);
				PreparedStatement stmt = con.prepareStatement(CMD_SEARCH_BY_NAME))
		{
			//"select * from \"Persons\" where \"UserName\" like '?' ";
			stmt.setString(1, name);
			ResultSet rs = stmt.executeQuery();

			while(rs.next())
			{
				person = getPerson(rs);				
//				person.setId(rs.getLong(1));
//				person.setUserName(rs.getString(2));
				//Kapat�lan sat�rlara alternatif oalrak yaz�ld�
			}
		}
		catch(SQLException sexc)
		{
			sexc.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return person;
	}
	
	public boolean deletePersonById(long id)
	{
		boolean status = false;
		try(Connection con = DriverManager.getConnection(m_url, m_username, m_password);
				PreparedStatement stmt = con.prepareStatement(CMD_DELETE_BY_ID))
		{
			stmt.setLong(1, id);
			 int temp = stmt.executeUpdate();
			 status = (temp == 1) ? true : false;
		}
		catch(SQLException sexc)
		{
			sexc.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return status;
	}
	
	public boolean deletePersonByName(String name)
	{
		boolean status = false;
		try(Connection con = DriverManager.getConnection(m_url, m_username, m_password);
				PreparedStatement stmt = con.prepareStatement(CMD_DELETE_BY_NAME))
		{
			stmt.setString(1, name);
			 int temp = stmt.executeUpdate();
			 status = (temp == 1) ? true : false;
		}
		catch(SQLException sexc)
		{
			sexc.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return status;
	}
	
	//�imdilik kullan�lm�yor. Bu y�zden tablonun yeni alanlar� dahil edilmedi
	public boolean updatePersonById(long id, String newUserName)
	{
		boolean status =false; 
		try(Connection con = DriverManager.getConnection(m_url, m_username, m_password);
				PreparedStatement stmt = con.prepareStatement(CMD_UPDATE_BY_ID))
		{
			//update \"Persons\" set \"UserName\" = '?' where \"PersonId\" = ?
			stmt.setString(1, newUserName);
			stmt.setLong(2, id);
			
			int temp = stmt.executeUpdate();
			status = (temp == 1) ? true : false;
			
		}
		catch(SQLException sexc)
		{
			sexc.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return status;
	}
	
	//�imdilik kullan�lm�yor. Bu y�zden tablonun yeni alanlar� dahil edilmedi
	public boolean updatePersonByName(String oldUserName, String newUserName)
	{
		boolean status =false; 
		try(Connection con = DriverManager.getConnection(m_url, m_username, m_password);
				PreparedStatement stmt = con.prepareStatement(CMD_UPDATE_BY_NAME))
		{
			//update "Persons" set "UserName" = '?' where "UserName"  like '?';
			stmt.setString(1, newUserName);
			stmt.setString(2, oldUserName);
			
			int temp = stmt.executeUpdate();
			status = (temp == 1) ? true : false;
			
		}
		catch(SQLException sexc)
		{
			sexc.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return status;
	}
	
	//"update \"Persons\" set \"UserName\" = ?, \"Password\" = ?, \"Email\" = ?, "
	//+ "\"CreationTime\" = ?, \"UpdatingTime\" = ? where \"PersonId\" = ?";
	public boolean updatePerson(long id, Person person)
	{

		boolean status = false;
		
		try(Connection con = DriverManager.getConnection(m_url, m_username, m_password);
				PreparedStatement stmt = con.prepareStatement(CMD_PERSON_UPDATE))
		{
			stmt.setString(1, person.getUserName());
			stmt.setString(2, person.getPassword());
			stmt.setString(3, person.getEmail());
			
			/*
				�NEML�:
				A�a��daki iflerin sebebi, insert'tekiyle ayn�.
				Date.valueOf() metodu, person'�n creationTime'� null ise null'�n de�erini almaya �al��acak. NPE
				else yazma sebebimiz ise:
				sql sorgusunda ? i�areti koyulan yere bir de�er girmesi gerekir ve 
				e�er if i�ine girmezse 4. ve 5. soru i�aretin alan� bo� kal�r. exeption
				bu ifler ve elseler ileride creation time ve updatetime dolu gelece�i i�in kald�r�lacak 
			 */
			if(person.getCreationTime() != null)
			{
				stmt.setDate(4, Date.valueOf(person.getCreationTime()));				
			}
			else
			{
				stmt.setDate(4, null);
			}
			if(person.getUpdatingTime() != null)
			{
				stmt.setDate(5, Date.valueOf(person.getUpdatingTime()));				
			}
			else
			{
				stmt.setDate(5, null);
			}
			stmt.setLong(6, id);
			status = (stmt.executeUpdate() == 1) ? true : false;
			
		}
		catch (SQLException sexc) {
			sexc.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return status;
	}
}
