package nebulaisAFunnyName.ScheduleDaytime.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import nebulaisAFunnyName.ScheduleDaytime.entity.PersonWork;

public class PersonWorkDao {
	private static final String INSERT_PERSONWORK = "insert into \"PersonWorks\" (\"PersonId\", \"WorkId\", \"Hour\") Values(?, ?, ?)"; 
	
	
	private static final PersonWorkDao ms_instance = new PersonWorkDao();
	private String m_url, m_userName, m_password;
	
	private PersonWorkDao() 
	{
		
	}
	
	public static PersonWorkDao instance(String url, String userName, String password)
	{
		ms_instance.m_url = url;
		ms_instance.m_userName = userName;
		ms_instance.m_password = password;
		
		return ms_instance;
	}	
	
	public Long insert(PersonWork personWork)
	{
		String[] generatedKey = {"Id"};
		long id = 0;
		
		try(Connection con = DriverManager.getConnection(m_url, m_userName, m_password);
				PreparedStatement stmt = con.prepareStatement(INSERT_PERSONWORK, generatedKey))
		{
			stmt.setLong(1, personWork.getPersonId());
			stmt.setLong(2, personWork.getWorkId());
			stmt.setInt(3, personWork.getHour());
			stmt.executeUpdate();
			
			ResultSet rs = stmt.getGeneratedKeys();
			while(rs.next())
			{
				id = rs.getLong(1); //�retti�imiz generedKey String dizisinden, ilk eleman(bizde tek eleman�) 
									//olan "Id" eleman�n� al�r.				
			}
			personWork.setId(id);
			
		}
		catch(SQLException sexc)
		{
			sexc.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
		return Long.valueOf(id);
		
	}
	
}
