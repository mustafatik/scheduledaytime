package nebulaisAFunnyName.ScheduleDaytime.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

import nebulaisAFunnyName.ScheduleDaytime.entity.Work;

public class WorkDao {
	//Singleton kullan�l�yor
	//https://www.youtube.com/watch?v=cEfjYynwooE
	private static final WorkDao ms_instance = new WorkDao();
	private String m_url, m_username, m_password;
	private WorkDao() {}
	
	private static final String CMD_GETALL_WORK = "select * from \"Works\" order by \"WorkId\"";
	private static final String CMD_GET_WORK_BY_ID = "select * from \"Works\" where \"WorkId\" = ?";
	private static final String CMD_DELETE_WORK_BY_ID = "delete from \"Works\" where \"WorkId\" = ?";
	private static final String CMD_UPDATE_WORK_NAME_BY_ID = "update \"Works\" set \"Name\" = ? where \"WorkId\" = ?";
	private static final String CMD_UPDATE_WORK_DATE_BY_ID = "update \"Works\" set \"Date\" = ? where \"WorkId\" = ?";
	private static final String CMD_UPDATE_WORK = "update \"Works\" set \"Name\" = ?, \"Date\" = ? where \"WorkId\" = ?";
	
	public static WorkDao getInstance(String url, String username, String password)
	{
		ms_instance.m_url = url;
		ms_instance.m_username = username;
		ms_instance.m_password = password;
		
		return ms_instance;
	}
	
	public Long /*veya long olacak karar veremedim*/ insert(Work work)
	{
		String cmd = "insert into \"Works\" (\"Name\", \"Date\") Values (?, ?)"; 
		
		String[] generatedKey = {"WorkId"};
		long id = 0;
		
		try(Connection con = DriverManager.getConnection(m_url, m_username, m_password);
				PreparedStatement stmt = con.prepareStatement(cmd, generatedKey))
		{
			stmt.setString(1, work.getName());
			//Hat�rlatma: sql paketindeki Date s�n�f�n�n statik valueOf metoduna LocalDate t�r�nden nesne verebiliyorsun
			//ve bunu prepaedStatemnt s�n�fn�n setDate metodu alg�layabiliyor
			stmt.setDate(2, Date.valueOf(work.getDate()));
			
			stmt.executeUpdate();
			
			ResultSet rs = stmt.getGeneratedKeys();
			if(rs.next())
			{
				id = rs.getLong(1);
			}
			
			work.setId(id);			
		}
		
		catch (SQLException e) {
			e.printStackTrace();
		}
		
		catch (Exception e) {
			e.printStackTrace();
		}
		
		return Long.valueOf(id);
	}
	
	public Work getWork(ResultSet rs) throws SQLException
	{
		//S�ralama de�i�ebilir
		Work w = new Work();
		w.setDate(rs.getDate(1).toLocalDate());
		w.setId(rs.getLong(2));
		w.setName(rs.getString(3));
		
		return w;
	}
	
	public ArrayList<Work> getAll()
	{
		ArrayList<Work> works = new ArrayList<Work>();
		
		try(Connection con = DriverManager.getConnection(m_url, m_username, m_password);
				PreparedStatement stmt = con.prepareStatement(CMD_GETALL_WORK))
		{
			ResultSet rs = stmt.executeQuery();
			
			while(rs.next())
			{
				works.add(getWork(rs));
			}
		}
		catch(SQLException sexc)
		{
			sexc.printStackTrace();
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return works;
	}
	
	public Work getWorkById(long id)
	{
		Work work = new Work();
		
		try(Connection con = DriverManager.getConnection(m_url, m_username, m_password);
				PreparedStatement stmt = con.prepareStatement(CMD_GET_WORK_BY_ID))
		{
			stmt.setLong(1, id);
			ResultSet rs = stmt.executeQuery();
			
			while(rs.next())
			{
				work = getWork(rs);
			}
		}
		catch(SQLException sexc)
		{
			sexc.printStackTrace();
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return work;
	}
	
	public boolean deleteWorkById(long id)
	{
		boolean status = false;
		try(Connection con = DriverManager.getConnection(m_url, m_username, m_password);
				PreparedStatement stmt = con.prepareStatement(CMD_DELETE_WORK_BY_ID))
		{
			stmt.setLong(1, id);
			
			status = (stmt.executeUpdate() == 1)? true : false;
		}
		catch(SQLException sexc)
		{
			sexc.printStackTrace();
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return status;
	}
	
	public boolean updateWorkNameById(long id, String newName)
	{
		boolean status = false;
		try(Connection con = DriverManager.getConnection(m_url, m_username, m_password);
				PreparedStatement stmt = con.prepareStatement(CMD_UPDATE_WORK_NAME_BY_ID))
		{
			//"update \"Works\" set \"Name\" = ? where \"WorkId\" = ?
			stmt.setString(1, newName);
			stmt.setLong(2, id);
			
			status = (stmt.executeUpdate() == 1)? true: false;
			
		}
		catch(SQLException sexc)
		{
			sexc.printStackTrace();
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return status;
		
	}
	
	public boolean updateWorkDateById(long id, LocalDate newDate)
	{
		boolean status = false;
		try(Connection con = DriverManager.getConnection(m_url, m_username, m_password);
				PreparedStatement stmt = con.prepareStatement(CMD_UPDATE_WORK_DATE_BY_ID))
		{
			//update \"Works\" set \"Date\" = ? where \"WorkId\" = ?
			stmt.setDate(1, Date.valueOf(newDate));
			stmt.setLong(2, id);
			
			status = (stmt.executeUpdate() == 1)? true: false;
			
		}
		catch(SQLException sexc)
		{
			sexc.printStackTrace();
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return status;
		
	}
	
	public boolean updateWork(long id, Work newWork)
	{
		boolean status = false;
		try(Connection con = DriverManager.getConnection(m_url, m_username, m_password);
				PreparedStatement stmt = con.prepareStatement(CMD_UPDATE_WORK))
		{
			//update \"Works\" set \"Name\" = ?, \"Date\" = ? where \"WorkId\" = ?
			stmt.setString(1, newWork.getName());
			//stmt.setDate(2, Date.valueOf(LocalDate.now())); �imdiki zaman� ge�mek istersen
			stmt.setDate(2, Date.valueOf(newWork.getDate()));
			stmt.setLong(3, id);
			status = (stmt.executeUpdate() == 1 )? true: false;
		}
		catch(SQLException sexc)
		{
			sexc.printStackTrace();
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return status;		
	}
}
