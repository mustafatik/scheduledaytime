package nebulaisAFunnyName.ScheduleDaytime.entity;

import java.time.LocalDate;

public class Person {
	private long id;
	private String userName;
	private String password;
	private String email;
	private LocalDate creationTime;
	private LocalDate updatingTime;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public LocalDate getCreationTime() {
		return creationTime;
	}
	public void setCreationTime(LocalDate creationTime) {
		this.creationTime = creationTime;
	}
	public LocalDate getUpdatingTime() {
		return updatingTime;
	}
	public void setUpdatingTime(LocalDate updatingTime) {
		this.updatingTime = updatingTime;
	}
}
