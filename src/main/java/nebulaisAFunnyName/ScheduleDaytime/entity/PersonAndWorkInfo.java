package nebulaisAFunnyName.ScheduleDaytime.entity;

import java.time.LocalDate;
//B�t�n elemanlar� i�inde bulundurur. Joinler i�in yard�mc� bir class gibi d���n�lebilir.
public class PersonAndWorkInfo {
	private Person m_person;
	private Work m_work;
	private PersonWork m_personWork;
	
	public long getPersonWorkId() {
		return m_personWork.getId();
	}
	public void setPersonWorkId(long id) {
		this.m_personWork.setId(id);
	}
	public long getPersonId() {
		return m_personWork.getPersonId();
	}
	public void setPersonId(long personId) {
		this.m_personWork.setPersonId(personId);
	}
	public long getWorkId() {
		return m_personWork.getWorkId();
	}
	public void setWorkId(long workId) {
		this.m_personWork.setWorkId(workId);
	}
	public int getHour() {
		return m_personWork.getHour();
	}
	public void setHour(int hour) {
		this.m_personWork.setHour(hour);
	}
	public String getUserName() {
		return m_person.getUserName();
	}
	public void setUserName(String userName) {
		m_person.setUserName(userName);
	}	
	public String getWorkName() {
		return m_work.getName();
	}
	public void setWorkName(String name) {
		this.m_work.setName(name);
	}
	public LocalDate getDate() {
		return m_work.getDate();
	}
	public void setDate(int year, int month, int day) {
		m_work.setDate(LocalDate.of(year, month, day));
	}
	//Test'i direkt localdate ile yapmak i�in ekledim ama ilerde de laz�m olabilir.
	public void setDate(LocalDate date) {
		this.m_work.setDate(date);
	}
	
}
