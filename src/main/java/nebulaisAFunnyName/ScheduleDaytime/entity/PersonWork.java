package nebulaisAFunnyName.ScheduleDaytime.entity;

public class PersonWork {
	private long m_id;
	private long m_personId;
	private long m_workId;
	private int m_hour;
	
	
	public long getId() {
		return m_id;
	}
	public void setId(long id) {
		this.m_id = id;
	}
	public long getPersonId() {
		return m_personId;
	}
	public void setPersonId(long personId) {
		this.m_personId = personId;
	}
	public long getWorkId() {
		return m_workId;
	}
	public void setWorkId(long workId) {
		this.m_workId = workId;
	}
	public int getHour() {
		return m_hour;
	}
	public void setHour(int hour) {
		this.m_hour = hour;
	}
	
	
	

}
