package nebulaisAFunnyName.ScheduleDaytime.entity;

import java.time.LocalDate;
import java.util.Set;

public class Work {
	private long id;
	private String name;
	private LocalDate date;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public LocalDate getDate() {
		return date;
	}
	
	public void setDate(int year, int month, int day) {
		date = LocalDate.of(year, month, day);
	}
	//Test'i direkt localdate ile yapmak i�in ekledim ama ilerde de laz�m olabilir.
	public void setDate(LocalDate date ) {
		this.date = date;
	}
	
}
