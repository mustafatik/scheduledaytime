<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<html>
	<head>
		<title>İş Ekleme Ekranı</title>
		<spring:url value="../work" var="url" htmlEscape="true"/>
	</head>
		
	<body>
		<form:form method="post" action="${url}/addedWork">
			<form:label path="name"> Name: </form:label>
			<form:input path="name"/>
			
			<form:label path="date"> Tarih: </form:label>
			<form:input path="date"/>
			
			<input type="submit" value="Ekle"/>
		</form:form>
	</body>
</html>
