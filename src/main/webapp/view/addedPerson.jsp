<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Kişi Başarıyla Eklendi</title>
</head>
<body>
	
	<table>
		<tr> 
			<td> id: </td>
			<td> ${id} </td> 
		</tr>
		<tr> 
			<td> UserName: </td>
			<td> ${userName}</td>
		</tr>	
		<tr> 
			<td> Password: </td>
			<td> ${password}</td>
		</tr>	
		<tr> 
			<td> Email: </td>
			<td> ${email}</td>
		</tr>	
		<tr> 
			<td> CreationTime: </td>
			<td> ${creationTime}</td>
		</tr>	
		<tr> 
			<td> UpdatingTime: </td>
			<td> ${updatingTime}</td>
		</tr>	
	</table>

</body>
</html>