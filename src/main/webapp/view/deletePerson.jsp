<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
	<head>
		<title>Kullanıcı Silme Ekranı</title>
		<spring:url value="../person" var="url" htmlEscape="true"/>
	</head>

	<body>
		<form:form method="POST" action= "${url}/deletedPerson">
			<form:label path="id"> Id: </form:label>
			<form:input path="id"/>
			<form:label path="userName"> Kullanıcı Adı: </form:label>
			<form:input path="userName"/>
			
			<input type="submit" value="Kullannıcıyı Sil"/>
		</form:form>	
	</body>
</html>