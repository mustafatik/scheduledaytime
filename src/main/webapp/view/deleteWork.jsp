<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
	<head>
		<title>İş Silme Ekranı</title>
		<spring:url value="../work" var="url" htmlEscape="true"/>
	</head>

	<body>
		<form:form action="${url}/deletedWork" method="post">
			<form:label path="id"> Id: </form:label>
			<form:input path="id"/>
			<%-- <form:label path="name"> İş Adı: </form:label>
			<form:input path="name"/>
			<form:label path="date"> Date: </form:label>
			<form:input path="date"/> --%>
			
			<input type="submit" value="İşi Sil"/>
		</form:form>	
	</body>
</html>