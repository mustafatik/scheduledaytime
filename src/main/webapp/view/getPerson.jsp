<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
 		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
		<script type="text/javascript" src='<c:url value="/resources/js/jquery-1.9.1.js" />'></script>
		<script type="text/javascript" src='<c:url value="/resources/js/bootstrap.js" />'></script>
		
		<style>
			.box.box-info {
			    border-top-color: #00c0ef;
			}
			.box-header {
			    color: #444;
			    				box-shadow: 0 1px 0px rgba(0,0,0,0.1);
			    				
			    position: relative;
			}
			.box-body {
			}
			.box {
				display: table;
			    position: center;
			    border-radius: 3px;
			    background: #ffffff;
				border-top: 3px solid #d2d6de;
			    border-top-color: rgb(210, 214, 222);
				width: 100%;
				box-shadow: 0 1px 1px rgba(0,0,0,0.1);
				box-sizing: border-box;
		</style>
		
		
		<title>Bulunan Kişi Bilgisi</title>
	</head>
	
	<body>
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-3">
				</div>
				<div class="col-md-6">
					<div class="box box-info">
						<div class="box-header with-border">
							<h3 class="box-title">Kullanıcı Bilgisi</h3>
							<br>
						</div>
					</div>
					
					<div class="box-body">
						<table class="table">
							<tbody>						
								<tr> 
									<td> Id: </td>
									<td> <c:out value="${id}"/></td> 
								</tr>
								<tr> 
									<td> UserName: </td>
									<td> <c:out value="${userName}"/></td>
								</tr>	
								<tr> 
									<td> Password: </td>
									<td> <c:out value="${password}"/></td>
								</tr>	
								<tr> 
									<td> Email: </td>
									<td> <c:out value="${email}"/></td>
								</tr>	
								<tr> 
									<td> CreationTime: </td>
									<td> <c:out value="${creationTime}"/></td>
								</tr>	
								<tr> 
									<td> UpdatingTime: </td>
									<td><c:out value="${updatingTime}"/></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>	
	</body>
</html>