<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<!-- TEMPLATE BU ADRESTEN -->
<!-- https://www.w3schools.com/bootstrap/tryit.asp?filename=trybs_temp_social&stacked=h -->

<!DOCTYPE html>
<html>
	<head>
	  <meta charset="utf-8">
	  <meta name="viewport" content="width=device-width, initial-scale=1">
	  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
	  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
	  
	  <jsp:include page="homeNavbar.jsp" />
	  <spring:url value="../person" var="url" htmlEscape="true"/>
	  
	  <style>    
	    /* Set black background color, white text and some padding */
	    footer {
	      background-color: #555;
	      color: white;
	      padding: 15px;
	    }
	  </style>
	</head>
	
	<body>
		
		<div class="container text-center">    
			<div class="row">
			    <div class="col-sm-3 well">
					<div class="well">
				        <p><a href="/ScheduleDaytime/admin/">Admin</a></p>
				        <img src="C:\Users\musta\Desktop\işleniyor\firstLogo.jpg" class="img-circle" height="65" width="65" alt="Avatar">
					</div>
				<div class="well">
			        <p><a href="#">Interests</a></p>
			        <p>
			          <span class="label label-default">News</span>
			          <span class="label label-primary">W3Schools</span>
			          <span class="label label-success">Labels</span>
			          <span class="label label-info">Football</span>
			          <span class="label label-warning">Gaming</span>
			          <span class="label label-danger">Friends</span>
			        </p>
				</div>
				<div class="alert alert-success fade in">
			        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
			        <p><strong>Uyarı Ekranı!</strong></p>
			        Admin ekranı düzeltilecek. Admin ekranı navbarı ve sol menünün template'ini içerecek.
			        <br/>Work ve person için sol menü ayarlanacak. 
			        <br/>Person'ın controller ve view'ındaki admin ifadesi düzeltilecek
				</div>
			      <p><a href="${url}/findPerson">Kullanıcı Bul</a></p>
			      <p><a href="${url}/person">Kullanıcı Ekle</a></p>
			      <p><a href="${url}/listPersons">Kullanıcıları Listele</a></p>
			      <p><a href="${url}/deletePerson">Kullanıcı Sil</a></p>
			      <p><a href="${url}/updatePerson">Kullanıcı Güncelle</a></p>
			      
<!-- 			      Aşağıdaki ifade yerine url değişkenine /ScheduleDaytime/admin/person ataması yapıldı -->
<!-- 			      <p><a href="/ScheduleDaytime/admin/person/findPerson">Kullanıcı Bul</a></p> -->
<!-- 			      <p><a href="/ScheduleDaytime/admin/person/person">Kullanıcı Ekle</a></p> -->
<!-- 			      <p><a href="/ScheduleDaytime/admin/person/listPersons">Kullanıcıları Listele</a></p> -->
<!-- 			      <p><a href="/ScheduleDaytime/admin/person/deletePerson">Kullanıcı Sil</a></p> -->
<!-- 			      <p><a href="/ScheduleDaytime/admin/person/updatePerson">Kullanıcı Güncelle</a></p> -->
			      
				</div>
			</div>  
		</div>
	</body>
</html>
