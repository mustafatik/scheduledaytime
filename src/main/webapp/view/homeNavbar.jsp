<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<!-- TEMPLATE BU ADRESTEN -->
<!-- https://www.w3schools.com/bootstrap/tryit.asp?filename=trybs_temp_social&stacked=h -->

<!DOCTYPE html>
<html>
	<head>
	  <meta charset="utf-8">
	  <meta name="viewport" content="width=device-width, initial-scale=1">
	  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
	  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
	  
	  <spring:url value=".." var="url" htmlEscape="true"/>
	  
	  <style>    
	    /* Set black background color, white text and some padding */
	    footer {
	      background-color: #555;
	      color: white;
	      padding: 15px;
	    }
	  </style>
	</head>
	
	<body>
		<!-- ÜST ÇUBUK -->
		<nav class="navbar navbar-inverse">
		  <div class="container-fluid">
		  	<!-- AŞAĞIDAKİ DİV'İN AMACI EKRAN BOYUTU KÜÇÜLTÜLDÜĞÜNDE NAVBAR ELAMNLARININ LİSTE BİÇİMİNDE AŞAĞI İNMESİNİ SAĞLAMAK-->
		    <div class="navbar-header"> 
		      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#NavbarId">
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>                        
		      </button>
		      <a class="navbar-brand" href="/ScheduleDaytime/admin/">ScheduleDayTime</a>
		    </div>
		    <div class="collapse navbar-collapse" id="NavbarId">
		      <ul class="nav navbar-nav">
		        <li class="active"><a href="${url}/person/">Person</a></li>
		        <li><a href="${url}/work/">Work</a></li>
		      </ul>
		      
		      <!-- ARMA BUTONU ŞİMDİLİK İŞLEVSİZ -->
		      <form class="navbar-form navbar-right" role="search">
		        <div class="form-group input-group">
		          <input type="text" class="form-control" placeholder="Search..">
		          <span class="input-group-btn">
		            <button class="btn btn-default" type="button">
		              <span class="glyphicon glyphicon-search"></span>
		            </button>
		          </span>        
		        </div>
		      </form>
		      
		      <!-- KULLANICI GİRİŞİ ETC ŞİMDİLİK İŞLEVSİZ -->
		      <ul class="nav navbar-nav navbar-right">
		        <li><a href="#"><span class="glyphicon glyphicon-user"></span> My Account</a></li>
		      </ul>
		    </div>
		  </div>
		</nav>	
	</body>
</html>