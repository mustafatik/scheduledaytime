<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>



<!DOCTYPE html>
<html>
<head>
	<title>İş Listesi</title>
</head>
<body>
	<table border="1">
		<tr>
			<th>WorkId</th>
			<th>WorkName</th>
			<th>Date</th>
		</tr>
		<c:if test="${works.size() > 0 }" >
			<c:forEach var="work" items="${works}">
				<tr>
					<td><c:out value="${work.id}" /></td>
					<td><c:out value="${work.name}"/></td>
					<td><c:out value="${work.date}" /></td>
				</tr>
			</c:forEach>
		</c:if>
		
		<c:if test="${works.size() == 0}">
				<c:out value="İş listesi boş"/>
		</c:if>
	</table>
</body>
</html>