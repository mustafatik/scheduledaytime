<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
 		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
		<script type="text/javascript" src='<c:url value="/resources/js/jquery-1.9.1.js" />'></script>
		<script type="text/javascript" src='<c:url value="/resources/js/bootstrap.js" />'></script>
		
<!-- 	https://stackoverflow.com/questions/5007210/how-to-use-springurl-with-an-a-tag -->
	  	<spring:url value="../person" var="url" htmlEscape="true"/>
		
		<style>
			.box.box-info {
			    border-top-color: #00c0ef;
			}
			.box-header {
			    color: #444;
			    				box-shadow: 0 1px 0px rgba(0,0,0,0.1);
			    				
			    position: relative;
			}
			.box-body {
				margin-top: 15px;
			}
			.box {
				display: table;
				padding: 15px;
			    position: center;
			    border-radius: 3px;
			    background: #ffffff;
				border-top: 3px solid #d2d6de;
			    border-top-color: rgb(210, 214, 222);
				margin-bottom: 20px;
				width: 100%;
				box-shadow: 0 1px 1px rgba(0,0,0,0.1);
				box-sizing: border-box;
		</style>
		
		
		<title>Kişi Ekleme Ekranı</title>
	</head>
		
	<body>
		<div class="container-fluid"> <!-- Veya bu: <div class="container"> -->

			<div class="row">
				<div class="col-md-4">
				</div>
				<div class="col-md-4">
					<div class="box box-info">
						<div class="box-header with-border">
							<h3 class="box-title">Kullanıcı Ekle</h3>
							<br>
						</div>
						
						<div class="box-body">
							<!-- KİLİT NOKTA ŞU: 
							FORM'U DOLDURMAK İÇİN HANGİ CONTROLLER'IN METOD'UNDAN GELİYORSAN O METOD'DAKİ MODALMAP'İ TEMSİL EDEN
							"string"'İN İSMİ İLE BURADAKİ modelAttribute'un ismi aynı olacak.
							Aynı olacak ki hangi modeli doldurması gerektiğini bilsin.-->
							<form:form  class="form-horizontal" modelAttribute="person" method="POST" action= "${url}/addPerson">
<!-- 						Örneğin bu form sonucu http://localhost:8080/ScheduleDaytime/admin/person/addPerson sayfasına gidecek-->
								<div class="form-group row"> 
									<form:label path="userName" class="col-sm-4 col-form-label">
										Kullanici Adi:
									</form:label>
									<div class="col-sm-8">
										<form:input path="userName" type="userName" class="form-control" id="userName" placeholder="Username" />
									</div>
								</div>	
								<div class="form-group row"> 
									<form:label path="password" class="col-sm-4 col-form-label">
										Şifre:
									</form:label>
									<div class="col-sm-8">
										<form:input path="password" type="password" class="form-control" id="password" placeholder="Password"/>
									</div>
								</div>
								<div class="form-group row"> 		 
									<form:label path="email" class="col-sm-4 col-form-label">
										Email Adresi:
									</form:label>
									<div class="col-sm-8">
										<form:input path="email" type="email" class="form-control" id="email" placeholder="Email" />
									</div>
								</div>			
								 <div class="text-right"> 
									<input type="submit" value="Ekle" class="btn btn-primary"/>
								</div>
							</form:form>
						</div>
					</div>	
				</div>
			</div>
		</div>
	</body>
</html>