<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>



<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
	<script type="text/javascript" src='<c:url value="/resources/js/jquery-1.9.1.js" />'></script>
	<script type="text/javascript" src='<c:url value="/resources/js/bootstrap.js" />'></script>
	
	<title>Kullanıcı Listesi</title>
	
	<style>
			.box.box-info {
			    border-top-color: #00c0ef;
			}
			.box-header {
			    color: #444;
			    				box-shadow: 0 1px 0px rgba(0,0,0,0.1);
			    				
			    position: relative;
			}
			.box-body {
				/* margin-top: 15px; */
			}
			.box {
				display: table;
				padding: 5px;
			    position: center;
			    border-radius: 3px;
			    background: #ffffff;
				border-top: 3px solid #d2d6de;
			    border-top-color: rgb(210, 214, 222);
				margin-bottom: 20px;
				width: 100%;
				box-shadow: 0 1px 1px rgba(0,0,0,0.1);
				box-sizing: border-box;
		</style>
		
</head>
	<body>
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-2">
				</div>
				<div class="col-md-8">
					<div class="box box-info">
						<div class="box-header with-border">
							<h3 class="box-title">Kullanıcı Listesi</h3>
							<br>
						</div>
						
						<div class="box-body">
							<table class="table">
								<thead>
									<tr>
										<th>
											ID
										</th>
										<th>
											UserName
										</th>
										<th>
											Password
										</th>
										<th>
											Email
										</th>
										<th>
											CreationTime
										</th>
										<th>
											UpdateTime
										</th>
										<th>
											Action
										</th>
									</tr>
								</thead>
								<tbody>						
									<c:if test="${persons.size() > 0 }" >
										<c:forEach var="person" items="${persons}">
											<c:choose>
												<c:when test="${persons.indexOf(person) % 4 == 0}">
													<tr class="table-active">
												</c:when>
												
												<c:when test="${persons.indexOf(person) % 4 == 1}">
													<tr class="table-success">
												</c:when>
												
												<c:when test="${persons.indexOf(person) % 4 == 2}">
													<tr class="table-warning">
												</c:when>
												
												<c:when test="${persons.indexOf(person) % 4 == 3}">
													<tr class="table-danger">
												</c:when>
												
												<c:otherwise>
													<tr>
												</c:otherwise>
											</c:choose>
												<td><c:out value="${person.id}" /></td>
												<td><c:out value="${person.userName}"/></td>
												<td><c:out value="${person.password}"/></td>
												<td><c:out value="${person.email}"/></td>
												<td><c:out value="${person.creationTime}"/></td>
												<td><c:out value="${person.updatingTime}"/></td>
												<td>
													<spring:url value="../person/updatePersons/${person.id}" var="updateUrl"></spring:url>
													<a class="btn btn-warning" href="${updateUrl}" role="button">Guncelle</a>
												</td>
													
												
												
												<!-- <a class="btn btn-primary" href="#" role="button">Sil</a>
												<a class="btn btn-primary" href="#" role="button">Güncelle</a> -->						
											</tr> <!-- Uyarı veriyor ama biz bunu choose-when(switch-case) içinde açtık -->
										</c:forEach>
									</c:if>		
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>	