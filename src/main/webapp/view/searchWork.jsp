<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
	<head>
		<title>İş Arama Ekranı</title>
		<spring:url value="../work" var="url" htmlEscape="true"/>
	</head>

	<body>
		<form:form action="${url}/findWork" method="post">
			<form:label path="id">Aranacak işin id değeri</form:label>
			<form:input path="id"/>
			
			<input type="submit" value="Ekle"/>
		</form:form>
	</body>
</html>