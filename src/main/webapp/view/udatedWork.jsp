<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<html>
	<head>
		<title>Güncellenen İş Bilgileri</title>
	</head>
	<body>
		<h1>Güncellenen Veriler</h1>
		
		<table>
			<tr> 
				<td> id: </td>
				<td> ${id} </td> 
			</tr>
			<tr> 
				<td> Work Name: </td>
				<td> ${name}</td>
			</tr>
			<tr> 
				<td> Work Date: </td>
				<td> ${date}</td>
			</tr>		
		</table>
	</body>
</html>