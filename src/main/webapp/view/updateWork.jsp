<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<html>
	<head>
		<title>İş Güncelleme Ekranı</title>
		<spring:url value="../work" var="url" htmlEscape="true"/>
	</head>
	
	<body>
		<form:form method="post" action="${url}/updatingWork">
			<form:label path="id"> Id: </form:label>
			<form:input path="id"/>
			
			<input type="submit" value="Güncellenecek İşi Seç"/>
		</form:form>
	
	</body>
</html>