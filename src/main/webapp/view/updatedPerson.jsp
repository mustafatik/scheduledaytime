<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<html>
	<head>
		<title>Güncellenen Kullanıcı Bilgileri</title>
	</head>
	<body>
		<h1>Güncellenen Veriler</h1>
		
		<table>
			<tr> 
				<td> id: </td>
				<td> ${id} </td> 
			</tr>
			<tr> 
				<td> UserName: </td>
				<td> ${userName}</td>
			</tr>	
			<tr> 
				<td> Password: </td>
				<td> ${password} </td> 
			</tr>
			<tr> 
				<td> Email: </td>
				<td> ${email}</td>
			</tr>	
			<tr> 
				<td> CreationTime: </td>
				<td> ${creationTime} </td> 
			</tr>
			<tr> 
				<td> UpdatingTime: </td>
				<td> ${updatingTime}</td>
			</tr>	
		</table>
	</body>
</html>