<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<html>
	<head>
		<title>Değiştirmek istediğiniz değeri değiştiriniz</title>
		<spring:url value="../person" var="url" htmlEscape="true"/>
	</head>
	
	<body>
		<form:form method="POST" action= "${url}/updatedPerson">
				<form:hidden path="creationTime"/> 
<!-- 				Değerin değişmeden kullanılabilmesi için hidden olarak geçtik -->
				<form:label path="id"> Id: </form:label>
				<form:input path="id"/>
				<form:label path="userName"> Kullanıcı Adı: </form:label>
				<form:input path="userName"/>
				<form:label path="password"> Password: </form:label>
				<form:input path="password"/>
				<form:label path="email"> Email: </form:label>
				<form:input path="email"/>
				
				<input type="submit" value="Guncellenen Degeri Onaylayin"/>
		</form:form>
	</body>
</html>