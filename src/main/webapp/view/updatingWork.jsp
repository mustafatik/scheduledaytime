<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<html>
	<head>
		<title>Değiştirmek istediğiniz değeri değiştiriniz</title>
		<spring:url value="../work" var="url" htmlEscape="true"/>
	</head>
	
	<body>
		<form:form method="post" action="${url}/updatedWork">
				<form:label path="id"> Id: </form:label>
				<form:input path="id"/>
				<form:label path="name"> Kullanıcı Adı: </form:label>
				<form:input path="name"/>
				<form:label path="date"> Tarih: </form:label>
				<form:input path="date"/>
				
				<input type="submit" value="Guncellenen Degeri Onaylayin"/>
		</form:form>
	</body>
</html>