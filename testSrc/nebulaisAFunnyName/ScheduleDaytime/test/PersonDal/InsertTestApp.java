package nebulaisAFunnyName.ScheduleDaytime.test.PersonDal;

import java.time.LocalDate;

import nebulaisAFunnyName.ScheduleDaytime.dal.PersonDal;
import nebulaisAFunnyName.ScheduleDaytime.entity.Person;

public class InsertTestApp {
	public static void main(String[] args) {
//		Work work = new Work();
//		work.setId(1);
//		HashSet<Work> w = new HashSet<Work>();
//		w.add(work);

		
		String userName = "kobeB";
		Person p  = new Person();
		p.setUserName(userName);
		p.setPassword("1234");
		p.setEmail("kobe@gmail.com");
		p.setCreationTime(LocalDate.of(2019, 12, 12));
		p.setUpdatingTime(LocalDate.now());
		PersonDal personDal = new PersonDal();
		long result = personDal.insertPerson(p);
		System.out.println(result);
	}
}
