package nebulaisAFunnyName.ScheduleDaytime.test.PersonDal;

import java.util.ArrayList;

import nebulaisAFunnyName.ScheduleDaytime.dal.PersonDal;
import nebulaisAFunnyName.ScheduleDaytime.entity.Person;

public class getAllTestApp {
	public static void main(String[] args)
	{
		ArrayList<Person> persons = new ArrayList<Person>();
		int count = 0;
		PersonDal persondal = new PersonDal();
		persons = persondal.getAllPerson();
		
		for(int i = 0; i < persons.size(); i++)
		{
			Person p = persons.get(i);
			System.out.printf("id: %d \t name: %s \t password: %s \t email: %s \t CreationTime: %s \t UpdatingTime: %s\n ", 
					p.getId(), p.getUserName(), p.getPassword(), p.getEmail(), p.getCreationTime(), p.getUpdatingTime());
			count++;
		}
		System.out.println(count);
	}
	
	
}
