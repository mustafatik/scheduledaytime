package nebulaisAFunnyName.ScheduleDaytime.test.PersonDal;

import nebulaisAFunnyName.ScheduleDaytime.dal.PersonDal;
import nebulaisAFunnyName.ScheduleDaytime.entity.Person;

public class getPersonByStringTestApp {
	public static void main(String[] args)
	{
		Person p = new Person();
		PersonDal pd = new PersonDal();
		String str = "kobeB";
		p = pd.getPersonByString(str);
		
		System.out.printf("id: %d \t name: %s \t password: %s \t email: %s \t CreationTime: %s \t UpdatingTime: %s\n ", 
				p.getId(), p.getUserName(), p.getPassword(), p.getEmail(), p.getCreationTime(), p.getUpdatingTime());
		
		
	}
}
