package nebulaisAFunnyName.ScheduleDaytime.test.PersonDal;

import java.time.LocalDate;

import nebulaisAFunnyName.ScheduleDaytime.dal.PersonDal;
import nebulaisAFunnyName.ScheduleDaytime.entity.Person;

public class updatePersonTestApp {

	public static void main(String[] args) {
		Person p = new Person();
		PersonDal pd = new PersonDal();
		
		p.setUserName("Kobe");
		p.setPassword("1234");
		p.setEmail("kobe@gmail.com");
		p.setCreationTime(LocalDate.of(2019, 12, 12));
		p.setUpdatingTime(LocalDate.now());
		
		System.out.println(pd.updatePerson(26, p));

	}

}
