package nebulaisAFunnyName.ScheduleDaytime.test.PersonWorkDal;

import nebulaisAFunnyName.ScheduleDaytime.dal.PersonWorkDal;
import nebulaisAFunnyName.ScheduleDaytime.entity.PersonWork;

public class InsertTestApp {
	public static void main(String [] args) 
	{
		PersonWork pw = new PersonWork();
		PersonWorkDal pwd = new PersonWorkDal();
		
		pw.setPersonId(2);
		pw.setWorkId(1);
		pw.setHour(11);
		
		long id = pwd.insert(pw);
		System.out.println(id);
	}

}
