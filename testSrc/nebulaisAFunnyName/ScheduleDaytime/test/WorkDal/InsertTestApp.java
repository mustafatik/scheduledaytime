package nebulaisAFunnyName.ScheduleDaytime.test.WorkDal;

import java.time.LocalDate;

import nebulaisAFunnyName.ScheduleDaytime.dal.WorkDal;
import nebulaisAFunnyName.ScheduleDaytime.entity.Work;

public class InsertTestApp {
	public static void main(String [] args) {
		LocalDate now = LocalDate.now();
		WorkDal workDal = new WorkDal();
		Work work = new Work();
		
		work.setName("calisma2");
		work.setDate(now);
		
		long result = workDal.insertWork(work);
		System.out.printf("Eklenen id de�eri : %d", result);
		
	}
}
