package nebulaisAFunnyName.ScheduleDaytime.test.WorkDal;

import nebulaisAFunnyName.ScheduleDaytime.dal.WorkDal;
import nebulaisAFunnyName.ScheduleDaytime.entity.Work;

public class getWorkByIdTestApp {

	public static void main(String[] args) {
		WorkDal wd = new WorkDal();
		Work w = wd.getWorkById(2);
		
		System.out.printf("%d \t %s \t %s ", w.getId(), w.getName(), w.getDate());
	}

}
