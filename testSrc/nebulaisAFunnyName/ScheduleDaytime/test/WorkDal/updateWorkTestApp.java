package nebulaisAFunnyName.ScheduleDaytime.test.WorkDal;

import java.time.LocalDate;

import nebulaisAFunnyName.ScheduleDaytime.dal.WorkDal;
import nebulaisAFunnyName.ScheduleDaytime.entity.Work;

public class updateWorkTestApp {
	public static void main(String[] args) {
		WorkDal wd = new WorkDal();
		Work w = new Work();
		w.setDate(LocalDate.now());
		w.setName("mvc");
		System.out.println(wd.updateWork(4, w));
	}

}
